#!/bin/sh
cd $CI_PROJECT_DIR
apt-get update
apt-get install -y libhdf5-dev
pip install -U -r requirements.txt
nosetests --with-coverage --cover-package=pymatreader && \
tox -c tox_ci.ini && \
codecov